import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    keys: [],
    formStatus: {},
    helps: {}
  },
  getters: {
    keys(state) {
      return state.keys;
    },
    formStatus(state) {
      return state.formStatus;
    },
    helps(state) {
      return state.helps;
    }
  },
  mutations: {
    setKey(state, key) {
      state.keys = [key];
    },
    initFormStatus(state, formStatus) {
      state.formStatus = formStatus;
    },
    initHelps(state, helps) {
      state.helps = helps;
    },
    validate(state, {fieldId, status, help}) {
      state.formStatus[fieldId] = status;
      state.helps[fieldId] = help;
    }
  },
  actions: {
    setKey({commit}, key) {
      commit('setKey', key);
    },
    initFormStatus({commit}, formStatus) {
      commit('initFormStatus', formStatus);
    },
    initHelps({commit}, helps) {
      commit('initHelps', helps);
    },
    validate({commit}, {fieldId, status, help}) {
      commit('validate', {fieldId, status, help});
    }
  }
})
