import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home'
import Index from './views/pages/Index'
import PageLayout from './views/pages/PageLayout'
import FormLayout from './views/pages/FormLayout'
import ListLayout from './views/pages/ListLayout'
import ApprovalFlow from './views/pages/ApprovalFlow'

import store from './store'

Vue.use(Router)

const router = new Router({
  routes: [
    {path: '/', component: Home, redirect: '/index', children: [
      {path: 'index', component: Index, meta: {key: 'index'}},
      {path: 'pagelayout', component: PageLayout, meta: {key: 'pagelayout'}},
      {path: 'formlayout', component: FormLayout, meta: {key: 'formlayout'}},
      {path: 'listlayout', component: ListLayout, meta: {key: 'listlayout'}},
      {path: 'approvalflow', component: ApprovalFlow, meta: {key: 'approvalflow'}},
    ]}
  ]
})

router.beforeEach((to, from, next) => {
  store.dispatch('setKey', to.meta.key);
  next();
})

export default router